#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

int VERBOSE = 0;

const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t ALL_DIGIT_COUNT = 36;

void arithmatoy_free(char *number) { free(number); }

char *arithmatoy_add(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "add: entering function\n");
  }
  // Retirer les zéros au début (genre 007)
  lhs = drop_leading_zeros(lhs);
  rhs = drop_leading_zeros(rhs);

  // Vérification que lhs > rhs
  size_t rhs_len = strlen(rhs);
  size_t lhs_len = strlen(lhs);

  if(lhs_len < rhs_len){
          const char* tmp = lhs;
          lhs = rhs;
          rhs = tmp;

          lhs_len = strlen(lhs);
  	  rhs_len = strlen(rhs);	
  }

  char *res = calloc(lhs_len + 1, sizeof(char));
  if(res == NULL){
          return NULL;
  }

  unsigned int carry = 0;
  unsigned int lhs_digit;
  unsigned int rhs_digit;
  unsigned int sum;
  char * result = calloc(lhs_len + 1, sizeof(char));
  for(size_t  i=0; i < lhs_len; i++){
          lhs_digit = get_digit_value(lhs[lhs_len - i -1]);
          rhs_digit = i < rhs_len ? get_digit_value(rhs[rhs_len - i -1]) : 0;
          sum = lhs_digit + rhs_digit + carry;
          carry = sum/base;
          sum = sum%base;
          result[lhs_len - i - 1] = to_digit(sum);
  }

  char * result_final;
  if(carry != 0){
  	result_final = calloc(lhs_len + 2, sizeof(char));
  	result_final[0] = to_digit(carry);
  	strcpy(result_final + 1, result);
  	arithmatoy_free(result);
  }
  else{

          result_final = result;
  }
  return result_final;
}
char *arithmatoy_sub(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "sub: entering function\n");
  }

  // Fill the function, the goal is to compute lhs - rhs (assuming lhs > rhs)
  // You should allocate a new char* large enough to store the result as a
  // string Implement the algorithm Return the result
  
lhs = drop_leading_zeros(lhs);
rhs = drop_leading_zeros(rhs);

const size_t lhs_len = strlen(lhs);
const size_t rhs_len = strlen(rhs);

// Si le lhs < rhs, le résultat sera négatif = erreur
if(lhs_len < rhs_len)
	return NULL;

// Allocation result +1 pour \0
char * result = calloc(lhs_len + 1, sizeof(char));

unsigned int lhs_digit, rhs_digit;
int diff;
unsigned int carry = 0;

for (size_t i = 0; i < lhs_len; i++){
	lhs_digit = get_digit_value(lhs[lhs_len - i - 1]);
	rhs_digit = i < rhs_len ? get_digit_value(rhs[rhs_len - i - 1]) : 0;
	diff = lhs_digit - rhs_digit - carry;

	if(diff < 0){
		carry = 1;
		diff += base;
	}
	else{
		carry=0;
	}

	result[lhs_len - i -1] = to_digit(diff);
}

	// S'il y a un reste, c'est que le résultat est négatif
	if (carry != 0){
		arithmatoy_free(result);
		return NULL;
	}

	// Formatage du résultat final
	
	char * result_bak = result;

	result = (char *) drop_leading_zeros(result);
	char * result_final = calloc(strlen(result) + 1, sizeof(char));
	strcpy(result_final, result);
	arithmatoy_free(result_bak);
	
	return result_final;


}

char *arithmatoy_mul(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "mul: entering function\n");
  }

  // Fill the function, the goal is to compute lhs * rhs
  // You should allocate a new char* large enough to store the result as a
  // string Implement the algorithm Return the result

  lhs = drop_leading_zeros(lhs);
  rhs = drop_leading_zeros(rhs);

  size_t lhs_len = strlen(lhs);
  size_t rhs_len = strlen(rhs);

  if (lhs_len < rhs_len) {
    // Inverse les arguments si lhs < rhs
    const char *tmp = lhs;
    lhs = rhs;
    rhs = tmp;

    lhs_len = strlen(lhs);
    rhs_len = strlen(rhs);
  }

  // Taille du résultat = somme des longueurs des 2 arguments
  const size_t result_len = lhs_len + rhs_len;
  char *result = calloc(result_len + 1, sizeof(char)); // +1 pour \0
  if (result == NULL) {
    debug_abort("mul: calloc failed\n");
  }
  char *partial_result = calloc(result_len + 1, sizeof(char));
  if (partial_result == NULL) {
    arithmatoy_free(result);
    debug_abort("mul: calloc failed\n");
  }


  // Multiplier chaque digit de rhs par chaque digit de lhs
  unsigned int lhs_digit, rhs_digit, product, carry;
  for (size_t i = 0; i < rhs_len; ++i) {
    // Remettre le résultat partiel à 0
    memset(partial_result, '0', result_len);
    // retenue
    carry = 0;

    rhs_digit = get_digit_value(rhs[rhs_len - i - 1]);
    if (VERBOSE) {
      fprintf(stderr, "mul: digit %c number %s\n", to_digit(rhs_digit), lhs);
    }

    // Mulitplier le digit de rhs par chaque digit de lhs
    for (size_t j = 0; j < lhs_len; ++j) {
      lhs_digit = get_digit_value(lhs[lhs_len - j - 1]);
      if (VERBOSE) {
        fprintf(
            stderr,
            "mul: digit %c digit %c carry %u\n",
            to_digit(rhs_digit),
            to_digit(lhs_digit),
            carry
        );
      }

      product = lhs_digit * rhs_digit + carry;
      carry = product / base;
      product = product % base;
      if (VERBOSE) {
        fprintf(
            stderr, "mul: result: digit %c carry %u\n", to_digit(product), carry
        );
      }

      partial_result[result_len - j - i - 1] = to_digit(product);
    }

    if (carry != 0) {
      if (VERBOSE) {
        fprintf(stderr, "mul: final carry %c\n", to_digit(carry));
      }

     // Si la dernière retenue != 0, ajouter un digit au résultat
      partial_result[result_len - lhs_len - i - 1] = to_digit(carry);
    }

    if (VERBOSE) {
      fprintf(
          stderr,
          "mul: add %s + %s\n",
          drop_leading_zeros(result),
	  // Retirer les 0 de tête sauf si le résultat est 0
	  // Si le résultat est 0, afficher 0
          partial_result + result_len - lhs_len - i - (carry != 0)
      );
    }

    // Ajouter le résultat partiel au résultat final
    char *tmp = arithmatoy_add(base, result, partial_result);
    if (tmp == NULL) {
      debug_abort("mul: arithmatoy_add failed\n");
    }

    arithmatoy_free(result);
    result = tmp;

    if (VERBOSE) {
      fprintf(stderr, "mul: result: %s\n", result);
    }
  }

  arithmatoy_free(partial_result);

  return result;
}






unsigned int get_digit_value(char digit) {
  // Convert a digit from get_all_digits() to its integer value
  if (digit >= '0' && digit <= '9') {
    return digit - '0';
  }
  if (digit >= 'a' && digit <= 'z') {
    return 10 + (digit - 'a');
  }
  return -1;
}

char to_digit(unsigned int value) {
  // Convert an integer value to a digit from get_all_digits()
  if (value >= ALL_DIGIT_COUNT) {
    debug_abort("Invalid value for to_digit()");
    return 0;
  }
  return get_all_digits()[value];
}

char *reverse(char *str) {
  // Reverse a string in place, return the pointer for convenience
  // Might be helpful if you fill your char* buffer from left to right
  const size_t length = strlen(str);
  const size_t bound = length / 2;
  for (size_t i = 0; i < bound; ++i) {
    char tmp = str[i];
    const size_t mirror = length - i - 1;
    str[i] = str[mirror];
    str[mirror] = tmp;
  }
  return str;
}

const char *drop_leading_zeros(const char *number) {
  // If the number has leading zeros, return a pointer past these zeros
  // Might be helpful to avoid computing a result with leading zeros
  if (*number == '\0') {
    return number;
  }
  while (*number == '0') {
    ++number;
  }
  if (*number == '\0') {
    --number;
  }
  return number;
}

void debug_abort(const char *debug_msg) {
  // Print a message and exit
  fprintf(stderr, debug_msg);
  exit(EXIT_FAILURE);
}
