# Aucun n'import ne doit être fait dans ce fichier


def nombre_entier(n: int) -> str:
    #Parcours de l'entier
   char = ""
   for i in range(n):
            char += 'S'
   char += "0"
   return char


# Entrée : "SS0"
# Sortie : "SSS0" (+1 S)
def S(n: str) -> str:
    char = 'S'
    char += n
    return char


# Entrée exemple : a = SS0
# b = SSS0
# Sortie : SSSSS0

def addition(a: str, b: str) -> str:
    if a == '0':
        return b
    elif a.startswith('S'):
        return S(addition(a[1:],b))

# Entrée exemple : a = SS0
# b = SS0
# Sortie : SSSS0
def multiplication(a: str, b: str) -> str:
    if a == '0':
        return '0'

    elif a.startswith('S'):
        return addition(b, multiplication(a[1:],b))
    


def facto_ite(n: int) -> int:
    
    if n == 0:
        return 1;
    res = 1
    while n != 1:
        res = n*res
        n-=1

    return res


def facto_rec(n: int) -> int:
    if n == 0:
        return 1
    else:
        return n*facto_rec(n-1)


def fibo_rec(n: int) -> int:
    if n==0:
        return 0
    if n==1:
        return 1
    return (fibo_rec(n-1)+fibo_rec(n-2))


def fibo_ite(n: int) -> int:
    if n==0:
        return 0
    elif n==1:
        return 1
    n1=0
    n2=1
    res=0
    for i in range (2, n+1, 1):
        res=n1+n2
        n1=n2
        n2=res

    return res

def golden_phi(n: int) -> int:
    return (fibo_rec(n+1) / fibo_rec(n))


def sqrt5(n: int) -> int:
    return (golden_phi(n) * 2)-1


def pow(a: float, n: int) -> float:
    
    if n == 0:
        return 1
    elif n %2 == 0:
        return pow(a**2, n//2)
    else:
        return a * pow(a**2, (n-1)//2)

